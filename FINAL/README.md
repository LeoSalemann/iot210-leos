# IOT Winter 2018 FINAL PROJECT
## Leo Salemann

## Simulating Philips Hue with a Raspberry Pi 3 and a SenseHAT

Based on [Philips Hue REST API](https://developers.meethue.com/documentation/lights-api)

multi_server.py provides a REST API that mimics a portion of Philips Hue
Supports: PUT and GET
Basic form:

curl <ip addr>:5000/api/leospi/lights/<id>/state -X PUT -d '{"on":on_bool}'

curl <ip addr>:5000/api/leospi/lights/<id>/state -X PUT -d '{"hue":H,"sat":S,"bri":B}'

curl <ip addr>:5000/api/leospi/lights/<id> -X GET


|parameter|min  |max|
|---------|-----|---|
|on_bool  |false|true|
|H        |0    |65535|
|S        |0    |254|
|B        |0    |254|


## Example calls
curl 192.168.1.8:5000/api/leospi/lights/1/state -X 'PUT' -d '{"hue":44418,"sat":220,"bri":76}' -H 'Content-Type: application/json' --verbose
curl 192.168.1.8:5000/api/leospi/lights/1/state -X 'PUT' -d '{"hue":64418,"sat":220,"bri":76}' -H 'Content-Type: application/json' --verbose
curl 192.168.1.8:5000/api/leospi/lights/1/state -X 'PUT' -d '{"on":true}' -H 'Content-Type: application/json' --verbose
curl 192.168.1.8:5000/api/leospi/lights/1/state -X 'PUT' -d '{"on":false}' -H 'Content-Type: application/json' --verbose
curl 192.168.1.8:5000/api/leospi/lights/1 -X 'GET' -H 'Content-Type: application/json' --verbose