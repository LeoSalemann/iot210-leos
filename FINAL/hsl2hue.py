#!/usr/bin/python
# =============================================================================
#        File : hsl2hue.py
# Description : Converts HSL colors from degrees, saturation %, level %
#      Author : Drew Gislsason
#        Date : 5/16/2017
# =============================================================================
import sys


class HSL_to_HUE(object):
  # Constructor
  # def __init__(self):
    
  def hsl2hue(self, hue,sat,bri):
    if hue >= 360: hue = 0
    if sat > 100:  sat = 100
    if bri > 100:  bri = 100

    hue = hue * 65536 / 360
    sat = sat * 254 / 100
    bri = bri * 254 / 100

    return hue,sat,bri

  def hue2hsl(self, hue,sat,bri):
    hue = float(hue) * 360.0/65536.0
    sat = float(sat) * 100.0/255.0
    bri = float(bri) * 100.0/255.0
    return hue,sat,bri


# =============================================================================
# main to test from CLI
def main():
  h2h = HSL_to_HUE()
  print (h2h.hsl2hue(20, 60, 49))
  print (h2h.hue2hsl(3640, 152, 124))

if __name__=="__main__":
  main()
