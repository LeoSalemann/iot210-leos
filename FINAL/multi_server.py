#!/usr/bin/python
# =============================================================================
#        File : multi_proto_server.py
# Description : mulitple protocol server support JSON, XML and Google Protobufs
#      Author : Leo Salemann
#        Date : 3/8/2018
#    Based on : multi_proto_server.py by  Drew Gislsason, 4/9/2017 
#
#     Details : Provides a REST API that Simualtes Philips Hue REST API
#               Requires Raspberry Pi 3 with SenseHAT
#               Simulates up to 64 separate Philips Hue light bulbs, 
#               each of which can be addressed via REST API
#               Supports on/off status and light color, expressed in 
#               Hue-style HSB format.
#
#  - **FUTURE**
#    - Add a GET command for returning all lights
#    - Add support for the new Hue api path
#    - Add more of the Hue API
#    - Add support for XML and Protobuf (if also supported by Philips)
# =============================================================================
import string
import json
import sys
import base64
import binascii
import xml.etree.ElementTree as ET

# import letter_pb2
# import simple_pb2

from flask import Flask, request
from sense import PiSenseHat
global LED_letter
global H # Hue
global S # Saturation
global B # brigtness
global is_off


# ============================== Data ====================================
PORT = 5000
content_json      = 'application/json'
content_xml       = 'application/xml'
content_protobufs = 'application/protobuf'

# Content-Types
type_JSON     = 0
type_XML      = 1
type_PROTOBUF = 2

# create Pi SenseHat Object
sense_hat = PiSenseHat()
LED_Letter= "" # used by led/api
H = [-1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1]

S = [-1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1]

B = [-1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1]

is_off = [True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True,
          True, True, True, True, True, True, True, True]

# ============================== Functions ====================================
def get_sensor_values():
    return sense_hat.getAllSensors()

# ============================== APIs ====================================
# create the global objects
app = Flask(__name__)

# ------------------- lights API -------------------------
# GET:    Retreive state of requested light.
# -----------------------------------------------------
@app.route("/api/leospi/lights/<lightid_str>", methods=['GET'])
def getHUE(lightid_str):
  global H
  global S
  global B
  global is_off

  lightid = int(lightid_str)
  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
      content_type = type_JSON

  rsp_data = ""
  # get (based on Content-Type)
  if request.method == 'GET':
    # Content type doesn't matter(?)
    input_valid = True
    if (is_off[lightid]):
      rsp_data += '{"on":false}'
    else:
      rsp_data += '{"on":true}'

    if (H[lightid] > 0):
      rsp_data +='{"hue":'+ str(H[lightid]) + "}"

    if (S[lightid] > 0):
      rsp_data +='{"sat":'+ str(S[lightid]) + "}"
    
    if (B[lightid] > 0):
      rsp_data +='{"bri":'+ str(B[lightid]) + "}"

    print ("rsp_data is ", rsp_data)
    return rsp_data, 200

# ------------------- lights API -------------------------
# PUT:    set an LED to a specific color (Hue HSB format)
# DELETE: Clear all pixels on the SenseHHAT
# -----------------------------------------------------
@app.route("/api/leospi/lights/<lightid_str>/state", methods=['PUT', 'DELETE'])
def HUEapi(lightid_str):
  global H
  global S
  global B
  global is_off

  rsp_data = []
  lightid = int(lightid_str)
  # assume JSON
  content_type = type_JSON
  if 'Content-Type' in request.headers:
    print "Content-Type: " + request.headers['Content-Type']
    if request.headers['Content-Type'] == content_xml:
      content_type = type_XML
    elif request.headers['Content-Type'] == content_protobufs:
      content_type = type_PROTOBUF
    elif request.headers['Content-Type'] == content_json:
      content_type = type_JSON

  if request.method == 'DELETE':  # Clear the LED screen 
    # Content type doesn't matter
    input_valid = True
    sense_hat.clearPixels()

  # put (based on Content-Type)
  elif request.method == 'PUT':
    input_valid = False
    turn_off = False
    

    # JSON:
    if content_type == type_JSON:
      data = request.get_json(force=True,silent=True)
      print ("----- here comes the data")
      print data
      print ("------ that was the data ----")
      if "on" in data:
        input_valid = True
        if data["on"] == True:
          is_off[lightid] = False
          print("  gonna turn it on >>>>>>>>>>>>>>>>>")
          rsp_data.append({"success":{"lights/state/on":True}})
        elif data["on"] == False:
          input_valid = True
          is_off[lightid] = True
          rsp_data.append({"success":{"lights/state/on":False}})
          sense_hat.setPixel(1, 0, 0, 0)
        else:
          input_valid = False
          badVal = data["on"]
          rsp_data.append({"success":{"lights/state/on":badVal}})
      if "hue" in data:
        if(0 <=  data["hue"] <= 65535):
          H[lightid] = int(data["hue"])
          input_valid = True
          rsp_data.append({"success":{"lights/state/hue":H[lightid]}})
        else:
          input_vaule = False
          bad_H = data["hue"]
          rsp_data.append({"failed":{"lights/state/hue":bad_H}})
      if "sat" in data:
        if(0 <= data["sat"] <= 254):
          S[lightid] = data["sat"]
          input_valid = True
          rsp_data.append({"success":{"lights/state/sat":S[lightid]}})
        else:
          bad_S = data["sat"]
          rsp_data.append({"failed":{"lights/state/sat":bad_S}})
      if "bri" in data:
        if(0 <= data["bri"] <= 254):
          B[lightid] = data["bri"]
          input_valid = True
          rsp_data.append({"success":{"lights/state/bri":B[lightid]}})
        else:
          bad_B = data["bri"]
          rsp_data.append({"failed":{"lights/state/bri":bad_B}})
        
      if (input_valid):
        print ("  inpput is VALID ............")
        if (is_off[lightid]):
          print ("  setting to zero ...................")
          sense_hat.setPixel_HUE(lightid, 0, 0, 0) 
        else:
          sense_hat.setPixel_HUE(lightid, H[lightid] , S[lightid], B[lightid])                

    # Future enhancment: XML support
    # elif content_type == type_XML:
    #   someday/oneday support XML

    # Future enhancment: Google protobuf support
    # elif content_type == type_PROTOBUF:
    #   someday/oneday support protobufs
  
  print ("rsp_data is ", rsp_data)
  return json.dumps(rsp_data), 200

# ============================== Main ====================================

if __name__ == "__main__":

  app.debug = True
  sense_hat.clearPixels()
  H = [-1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1]

  S = [-1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1]

  B = [-1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1]

  is_off = [True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True,
            True, True, True, True, True, True, True, True]

  app.run(host='0.0.0.0', port=PORT)
