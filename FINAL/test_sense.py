#!/usr/bin/python
# =============================================================================
#        File : test_sense.py
# Description : Test harness for sense.py
#      Author : Leo Salemann
#        Date : 1/18/2018
# =============================================================================
from sense import PiSenseHat
import time

# create Pi SenseHat Object
sense_hat = PiSenseHat()

print ("===================< BEGIN ENVIRONMENTAL METHODS >===================")

print ("  Get all environment values.")
print (sense_hat.getAllSensors())

print ("================< ENVIRONMENTAL METHODS COMPLETE >===================")
print ("")

print ("===================< BEGIN LED METHODS >===================")

print ("  Clear pixels")
sense_hat.clearPixels()
time.sleep(5)

# sense_hat.setPixel(3, 255, 255, 255)
# sense_hat.clearPixels()
sense_hat.setPixel    (0, 200, 100, 50)
sense_hat.setPixel_HSL(1, 20, 0.60, 0.49)
sense_hat.setPixel_HUE(2,3640, 152, 124)
# for id in range(64):
#     # sense_hat.clearPixels()
#     sense_hat.setPixel(id, 100+id, 2*id, 3*id)
#     print (id)
#     time.sleep(0.2)
#     # sense_hat.clearPixels()

time.sleep(5)
# sense_hat.clearPixels()

# print ("  Display the letter J") 
# sense_hat.printLetter("J")

print ("================< LED METHODS COMPLETE >===================")