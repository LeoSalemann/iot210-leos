#!/usr/bin/python
# =============================================================================
#        File : test_hsl2hue.py
# Description : Test harness for hsl2hue.py
#      Author : Leo Salemann
#        Date : 3/2/2018
# =============================================================================
from hsl2hue import HSL_to_HUE


# create Pi SenseHat Object
h2h = HSL_to_HUE()

h2h.hsl2hue(271, 68, 32)
