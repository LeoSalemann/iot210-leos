#!/usr/bin/python
# =============================================================================
#        File : dbox_logger.py
#      Source : based on dropbox_sha256.py by Drew Gislsason, 4/6/17
# Description : allows creating a password database on dropbox
#      Author : Leo Salemann
#        Date : 2/22/18
# =============================================================================
# built-in libraries
import base64
import getpass
import sys
import string
import os

# installed libraries
from Crypto.Hash import SHA256
import dropbox

# See: http://md5decrypt.net/en/Sha256/
# //www.dropbox.com/developers/apps/info/appkey

# TOKEN    = 'replace with file content'
FILENAME = '/passwords.txt'
SALT     = '' # '$n3@_'
INITIAL  = 'hello,486ea46224d1bb4fb680f34f7c9ad96a8f24ec88be73ea8e5a6c65260e9cb8a7\n'

DEBUG    = 0

STATUS_OK         = 0
STATUS_FAILED     = 1
STATUS_DUPLICATE  = 2

TMPFILE = "x.txt"

# login using secure token
def dropbox_login(token):
  dbx = dropbox.Dropbox(token)
  try:
    info = dbx.users_get_current_account()
    return dbx
  except:
    print "not a valid token"
    return None

# get the whole file from dropbox into memory as a string
def dropbox_getfile(dbx, filename):
  try:
    os.remove(TMPFILE)
  except:
    print "no tmp file"
  try:
    dbx.files_download_to_file(TMPFILE,filename)
    f = open(TMPFILE)
    content = f.read()
    f.close()
    return content
  except:
    print "could not download file"
    return None

# write a dropbox file, given entire file contents.
# Returns size of file, or 0 if failed
def dropbox_putfile(dbx, filename, filecontents):
  try:
    dbx.files_delete(filename)
  except:
    print "no file to delete"
  try:
    dbx.files_alpha_upload(filecontents, filename)
    return len(filecontents)
  except:
    print "failed to upload"
    return 0

# send a file to drop box
def sendto_dropbox(dbx, filename):
  with open(filename, 'r') as theFile:
    contents = theFile.read()
  dropbox_putfile(dbx, filename, contents)


# if username matches, return password sha256
def iot_find_password_sha(uname, filecontents):

  # empty file or user, not found
  if uname == None or filecontents == None:
    return None

  # look through all the lines
  sha = None
  lines = filecontents.split('\n')
  for line in lines:
    if DEBUG: print "line " + str(line)
    i = string.find(line, ',')
    if i > 0 and uname == line[0:i]:
      sha = line[i+1:]
  return sha

# create a user with hashed password
# returns status,filecontents
def iot_create_account(filecontents):

  # don't allow creating the same name twice
  uname = raw_input("Enter username to create: ")
  if iot_find_password_sha(uname, filecontents):
    return STATUS_DUPLICATE, filecontents

  # get username and password in one line
  pwd   = SALT + getpass.getpass("Enter password: ")
  sha1  = SHA256.new(pwd).hexdigest()
  line  = uname + ',' + sha1 + '\n'

  # append it to the file
  filecontents = filecontents + line
  if DEBUG: print filecontents
  return STATUS_OK, filecontents

# login a user with hashed password
def iot_login_account(uname, upwd, filecontents):

  if DEBUG: print "uname " + str(uname)
  if DEBUG: print "file  " + str(filecontents)

  # get sha from password
  pwd   = SALT + upwd
  sha1  = SHA256.new(pwd).hexdigest()
  if DEBUG: print "sha1: " + str(sha1)

  # login if the sha matches
  sha2  = iot_find_password_sha(uname, filecontents)
  if DEBUG: print "sha2: " + str(sha2)
  if sha2 == sha1:
    return True
  return False

# main function
def main():
  print len(sys.argv), 'arguments'
  if len(sys.argv) != 4:
    print 'dbox_logger.py <username> <password> <tokenfile>'
    exit(1)
  else:
    uname = sys.argv[1]
    pw = sys.argv[2]
    tokenfile = sys.argv[3]

    with open(tokenfile, 'r') as theTokenFile:
      TOKEN = theTokenFile.read().replace('\n', '')

    print 'user is', uname, '; pw is', pw, '; token is', TOKEN

    if len(TOKEN) <= len("{token}"):
      print "\nLooks like you didn't add your token. Go generate one for"
      print "your app API at: https://www.dropbox.com/developers/apps\n"

    # login to dropbox
    dbx = dropbox_login(TOKEN)
    if dbx:
      print "\nlogged into dropbox"
      filecontents = dropbox_getfile(dbx, FILENAME)
      if not filecontents:
        filecontents = INITIAL
        dropbox_putfile(dbx, FILENAME, filecontents)
      print "sending syslog file ..."
      sendto_dropbox(dbx, '/var/log/syslog')
      print "... sent"
    else:
      print "\nfailed to log in to dropbox!"
      exit(1)

    # if iot_login_account(uname, pw, filecontents):
    #   print "Welcome " + uname + "! Logged in"
    # else:
    #   print "Login failed!!"

    exit(0)

if __name__ == "__main__":
  main()
