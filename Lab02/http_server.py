#!/usr/bin/python
# =============================================================================
#        File : http_server.py
# Description : HTTP server echos the verb and input
#      Author : Drew Gislsason
#        Date : 4/6/2017
# =============================================================================
import random
import string
import json
import sys
from sense import PiSenseHat
import datetime

PORT = 5000

from flask import Flask, request

# create Pi SenseHat Object
sense_hat = PiSenseHat()
LED_Letter = "" # used by led/api

# ============================== Functions ====================================
def get_sensor_values():
    return sense_hat.getAllSensors()

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

# ------------------- ORIGINAL SAMPLE API -------------------------
@app.route("/my/api", methods=['GET', 'PUT', 'POST', 'DELETE'])
def ApiV1Echo():

  rsp_data = "Echo:\nMethod: "

  if request.method == 'GET':
    rsp_data += "GET"
  elif request.method == 'PUT':
    rsp_data += "PUT"
  elif request.method == 'POST':
    rsp_data += "POST"
  elif request.method == 'DELETE':
    rsp_data += "DELETE"

  rsp_data += "\nArgs: "
  for arg in request.args:
    rsp_data += str(arg) + ":" + request.args[arg] + " "

  rsp_data += "\nData:\n"
  rsp_data += request.get_data()
  rsp_data += "\n"

  return rsp_data, 200

# ------------------- LED API -------------------------
# POST:   Send a letter to the screen for printing
# GET:    Retreive current letter
# PUT:    Change to new letter
# DELETE: Clear the screen
# -----------------------------------------------------
@app.route("/led/api", methods=['GET', 'PUT', 'POST', 'DELETE'])
def LEDapi():

  rsp_data = "Echo:\nMethod: "

  if request.method == 'GET':       # Retreive current letter
    rsp_data += "GET"
    rsp_data += " "
    rsp_data += str(LED_Letter)
  elif request.method == 'PUT':     # Update current letter
    rsp_data += "PUT"
    for arg in request.args:
      global LED_Letter
      LED_Letter = request.args[arg]
      print("arg is " + arg  + 
            ", val is " +  request.args[arg] + 
            ", LED_Letter= " + LED_Letter)
      sense_hat.printLetter(LED_Letter)
  elif request.method == 'POST':    # Send a new letter
    rsp_data += "POST"
    # extract the data payload 
    global LED_Letter
    LED_Letter = request.get_data()
    print("LED_Letter= " + LED_Letter)
    sense_hat.printLetter(LED_Letter)
  elif request.method == 'DELETE':  # Clear the LED screen
    LED_Letter = ""
    rsp_data += "DELETE"
    sense_hat.clearPixels()

  rsp_data += "\nArgs: "
  for arg in request.args:
    rsp_data += str(arg) + ":" + request.args[arg] + " "

  rsp_data += "\nData:\n"
  rsp_data += request.get_data()
  rsp_data += "\n"

  return rsp_data, 200

# ------------------- ENVIRONMENT API -------------------------
# GET: retreive all enviroment variables, as a json doc
# -------------------------------------------------------------
@app.route("/env/api", methods=['GET'])
def ENVapi():

  if request.method != 'GET':
    return 404

  # TO-DO: Args for getting different sensor params

  data_obj = get_sensor_values()
  # yield('data: {0}\n\n'.format(json.dumps(data_obj)))
  rsp_data = json.dumps(data_obj)

  return rsp_data, 200
# ============================== Main ====================================

if __name__ == "__main__":

  print "HTTP Server"

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
