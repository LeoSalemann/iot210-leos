#!/usr/bin/python

# =============================================================================
#        File : sense.py
# Description : Driver interface for SenseHat Module
#      Author : S. Dame
#        Date : 2/13/2017
# =============================================================================
#
#  Official sense-hat API available from :
#  http://pythonhosted.org/sense-hat/api/
#
# =============================================================================

from sense_hat import SenseHat
from datetime import datetime
import pprint
import json

# ============================== GLOBAL VARIABLES ==================================

# -----------------------< color nicknames >-------------------
r = [255, 0, 0]
o = [255, 127, 0]
y = [255, 255, 0]
g = [0, 255, 0]
b = [0, 0, 255]
i = [75, 0, 130]
v = [159, 0, 255]
e = [0, 0, 0] # e is for empty (not black, b already used for blue)
w = [255, 255, 255]

# -----< handy for blanking out the screen between images >-------
blank_image = [
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e ]

class PiSenseHat(object):
	"""Raspberry Pi 'IoT Sense Hat API Driver Class'."""

	# Constructor
	def __init__(self):
		self.sense = SenseHat()
		# enable all IMU functions
		self.sense.set_imu_config(True, True, True)

	# Pressure
	def getPressure(self):
		return self.sense.get_pressure()

	# Temperature
	def getTemperature(self):
		return self.sense.get_temperature()

	# Humidity
	def getHumidity(self):
		return self.sense.get_humidity()

	def getHumidityTemperature(self):
		return self.sense.get_temperature_from_humidity()

	def getPressureTemperature(self):
		return self.sense.get_temperature_from_pressure()

	def getOrientationRadians(self):
		return self.sense.get_orientation_radians()

	def getOrientationDegrees(self):
		return self.sense.get_orientation_degrees()

	# degrees from North
	def getCompass(self):
		return self.sense.get_compass()

	def getAccelerometer(self):
		return self.sense.get_accelerometer_raw()

	def getEnvironmental(self):
		sensors = {'name' : 'sense-hat', 'environmental':{}}
		return sensors

	def getJoystick(self):
		sensors = {'name' : 'sense-hat', 'joystick':{}}
		return sensors

	def getInertial(self):
		sensors = {'name' : 'sense-hat', 'inertial':{}}

	def printLetter(self, Letter):
		self.sense.show_letter(Letter)

	def clearPixels(self):
		self.sense.set_pixels(blank_image)
	
	def getAllSensors(self):
		sensors = {'name' : 'sense-hat', 'environmental':{}, 'inertial':{}, 'joystick':{}}
		sensors['environmental']['pressure'] = { 'value':self.sense.get_pressure(), 'unit':'mbar'}
		sensors['environmental']['temperature'] = { 'value':self.sense.get_temperature(), 'unit':'C'}
		sensors['environmental']['humidity'] = { 'value':self.sense.get_humidity(), 'unit': '%RH'}
		accel = self.sense.get_accelerometer_raw()
		sensors['inertial']['accelerometer'] = { 'x':accel['x'], 'y':accel['y'], 'z': accel['z'], 'unit':'g'}
		orientation = self.sense.get_orientation_degrees()
		sensors['inertial']['orientation'] = { 'compass':self.sense.get_compass(), 'pitch':orientation['pitch'], 'roll':orientation['roll'], 'yaw': orientation['yaw'], 'unit':'degrees'}
		sensors['meas_time'] = datetime.now().isoformat()

		for event in self.sense.stick.get_events():
			sensors['joystick'] = {'action':json.dumps(event.action), 'direction':json.dumps(event.direction)}			
			break

		return sensors


# =============================================================================
# main to test from CLI
def main():

    # create an instance of my pi sense-hat sensor object
    pi_sense_hat = PiSenseHat()

    # Read Parameters.
    p = pi_sense_hat.getPressure()
    t_c = pi_sense_hat.getTemperature()
    h = pi_sense_hat.getHumidity()
    ht = pi_sense_hat.getHumidityTemperature()
    hp = pi_sense_hat.getPressureTemperature()
    orientation = pi_sense_hat.getOrientationDegrees()
    accel = pi_sense_hat.getAccelerometer()
    d = pi_sense_hat.getCompass()

    print("================ Discrete Sensor Values ==================")
    print "      Pressure :", p
    print "   Temperature :", t_c
    print "      Humidity :", h
    print "  HumidityTemp :", ht
    print "  PressureTemp :", hp
    print "       Compass :", d
    print("  p: {pitch}, r: {roll}, y: {yaw}".format(**orientation))
    print("  x: {x}, y: {y}, z: {z}".format(**accel))
    print("==========================================================\n")

    print("================== Dictionary Object =====================")
    pprint.pprint(pi_sense_hat.getAllSensors())
    print("==========================================================\n")
    pi_sense_hat.clearPixels()

if __name__=="__main__":
	main()


