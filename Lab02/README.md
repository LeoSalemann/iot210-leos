# HW2 REST API
## Leo Salemann

/env/api Examples:

  GET /env/api  # returns sensor values in json format
 
/led/api Examples:

  POST /led/api X        # screen prints X

  GET  /led/api          # retreives current letter

  PUT  /led/api?letter=Y # changes letter to Y

  DELETE /led/api        # clears the screen
 
  exit - leave the program\n