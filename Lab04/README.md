# HW4 WAP Server
## Leo Salemann

### Overview
Basic WAP setup based on [Setting up a Raspberry Pi as a WiFi access point](https://learn.adafruit.com/setting-up-a-raspberry-pi-as-a-wifi-access-point/overview)
article from https://learn.adafruit.com/

See adafruit folder in this repo; replicate at your root level

/env/api
Get temperature, pressure, humidy readings
  curl <RPi url>:5000/env/api -X GET

/led/api
set LED to display a question mark
  curl <RPi url>:5000/led/api -X POST -d '?' --verbose

get character currently displayed on LED
  curl <RPi url>:5000/env/api -X GET --verbose

change LED to displa an exclamation point
  curl <RPi url>:5000/led/api?letter=! -X PUT -d  --verbose

clear the screen
  curl <RPi url>:5000/env/api -X DELET --verbose
  	