# HW7 Bluetooth Low Energy (BLE) Beacon 
## Leo Salemann

1. Pick a favorite webiste.
1. Generate a tiny url for it
1. Use [Eddystone URL command calculator](https://yencarnacion.github.io/eddystone-url-calculator/) to get the hex dump.
1. Copy the sudo hci controls, execute in your Pi. Should be something like this:

```
$ sudo hciconfig hci0 up

$ sudo hciconfig hci0 leadv 3

$ sudo hcitool -i hci0 cmd 0x08 0x0008 1e 02 01 06 03 03 aa fe 16 16 aa fe 10 00 03 74 69 6e 79 75 72 6c 00 79 63 75 72 73 37 72 6b 00
```

You should be able to review the result on an app such as PhyWeb