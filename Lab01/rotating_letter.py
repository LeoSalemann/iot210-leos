##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT210
# Lab 1: Sense Hat, from Essentials_SensHAT_v1.pdf
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 1/9/18

# Code snippets based on, "Experiment with the Sense Hat", 
# The MagPi Essenstials, by The Pi Education Team
# rasberrypi.org/magpi
##############################################################

from sense_hat import SenseHat
sense = SenseHat()
sense.show_letter("J")
while True:
    x, y, z = sense.get_accelerometer_raw().values()
    x = round(x, 0)
    y = round(y, 0)

    # check x and y rotations, look for +/- 1  
    # Use shoudl be able to 'tilt' RPi on any edge, and have
    # the letters auto-orient to establish "up"
    if x == -1:
        sense.set_rotation(180)
    elif x == 1:
        sense.set_rotation(0)
    elif y == 1:
         sense.set_rotation(270)
    elif y == -1:
         sense.set_rotation(90)
    
    # else:
    #     sense.set_rotation(0)

    print("x=%s, y=%s, z=%s" % (x, y, z))