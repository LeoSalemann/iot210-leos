
##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT210
# Lab 1: Sense Hat, from Essentials_SensHAT_v1.pdf
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 1/9/18

# Code snippets based on, "Experiment with the Sense Hat", 
# The MagPi Essenstials, by The Pi Education Team
# rasberrypi.org/magpi
##############################################################

from sense_hat import SenseHat
import time  # pg 27
sense = SenseHat()

# ============================== GLOBAL VARIABLES ==================================

# -----------------------< color nicknames (pg 24) >-------------------
r = [255, 0, 0]
o = [255, 127, 0]
y = [255, 255, 0]
g = [0, 255, 0]
b = [0, 0, 255]
i = [75, 0, 130]
v = [159, 0, 255]
e = [0, 0, 0] # e is for empty (not black, b already used for blue)
w = [255, 255, 255]
# -----------------------< example image (pg 25) >-------------------
image = [ 
    e,e,e,e,e,e,e,e,
    e,e,e,r,r,e,e,e,
    e,r,r,o,o,r,r,e,
    r,o,o,y,y,o,o,r,
    o,y,y,g,g,y,y,o,
    y,g,g,b,b,g,g,y,
    b,b,b,i,i,b,b,b,
    b,i,i,v,v,i,i,b ]

# -----< My own image (sun, cloud, flower), helps orient the pi on startup >----
sunny_day = [
    y,y,b,b,b,w,w,b,
    y,y,b,b,w,w,w,w,
    b,b,b,v,b,w,w,b,
    b,b,v,v,v,b,b,b,
    b,b,b,v,b,b,b,b,
    b,b,g,g,g,b,b,b,
    b,b,b,g,b,b,b,b,
    g,g,g,g,g,g,g,g ]

# -----< My own image (sun, cloud, flower), helps orient the pi on startup >----
tree_image1 = [
    y,y,b,b,b,b,b,b,
    y,b,b,g,g,b,b,b,
    b,b,g,g,g,g,b,b,
    b,g,b,g,g,b,g,b,
    b,b,g,g,g,g,b,b,
    b,g,b,g,g,b,g,b,
    b,b,b,g,g,b,b,b,
    g,g,g,g,g,g,g,g ]

tree_image2 = [
    y,y,b,b,b,b,b,b,
    y,b,b,g,b,b,b,b,
    b,b,g,g,g,b,b,b,
    b,b,b,g,b,b,b,b,
    b,b,g,g,g,b,b,b,
    b,g,g,g,g,g,b,b,
    b,b,b,g,b,b,b,b,
    g,g,g,g,g,g,g,g ] 

tree_image3 = [
    y,y,b,b,w,w,b,b,
    y,b,b,w,w,w,w,b,
    b,b,b,b,w,w,b,b,
    b,b,b,g,b,b,b,b,
    b,b,g,g,g,b,b,b,
    b,g,g,g,g,g,b,b,
    b,b,b,g,b,b,b,b,
    g,g,g,g,g,g,g,g ]   

tree_image4 = [
    y,y,b,b,b,b,b,b,
    y,b,b,b,b,b,w,b,
    b,b,w,b,g,b,b,b,
    w,b,b,g,g,g,b,b,
    b,b,g,g,g,g,g,b,
    b,g,g,g,g,g,g,g,
    b,b,b,g,g,g,b,b,
    g,g,g,g,g,g,g,g ]   

tree_image = [
    y,y,b,b,b,b,b,b,
    y,b,b,b,b,b,b,b,
    b,b,b,b,g,b,b,b,
    b,b,b,g,g,g,b,b,
    b,b,g,g,g,g,g,b,
    b,g,g,g,g,g,g,g,
    b,b,b,g,g,g,b,b,
    g,g,g,g,g,g,g,g ]   
                      

# -----< handy for blanking out the screen between images >-------
blank_image = [
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e ]

sleep_time = 0.5  # Handy for pausing the screen between iamges

UP = 0 # For orienting text display.

# ============================== GLOBAL VARIABLES ==================================


# sense.set_rotation(180) # pg 26

# Show the sun/ground/fower image, allow time for user to see which side is up
# and orient the Pi properly
sense.set_pixels(tree_image)
time.sleep(1.5)

# New stuff, based on acellerometer passage in pp31-32
for ticks in range (0,100):
    x, y, z = sense.get_accelerometer_raw().values()
    x = round(x, 0)
    y = round(y, 0)

    # check x and y rotations, look for +/- 1  
    # Use shoudl be able to 'tilt' RPi on any edge, and have
    # the letters auto-orient to establish "up"
    if x == -1:
        UP = 180
    elif x == 1:
        UP = 0
    elif y == 1:
        UP = 270
    elif y == -1:
        UP = 90

    sense.set_rotation(UP)

# pg 23 --------------------------------------------------------
sense.show_message("Astro Pi is awesome!", scroll_speed=0.04, 
                   text_colour=[255,255,0], back_colour=[0,0,255])

# pg 24 --------------------------------------------------------
sense.set_pixel(0, 2, [0, 255, 0])
sense.set_pixel(7, 4, [255, 0, 0])
time.sleep(sleep_time)

# pg 25 --------------------------------------------------------
sense.set_pixels(image)
time.sleep(sleep_time)

# clear the screen (not in MagPi article) ------------------
sense.set_pixels(blank_image)
time.sleep(sleep_time)

# pg 27 ----------------------------------------------------
sense.show_letter("J")
angles = [0, 90, 180, 270, 0, 90, 180, 270]
for r in angles:
 sense.set_rotation(r)
 time.sleep(0.5)

# pp 27-28 -------------------------------------------------
flip_image = [
e,e,e,e,e,e,e,e,
e,e,e,e,e,e,e,e,
w,w,w,e,e,w,w,w,
w,w,b,e,e,w,w,b,
w,w,w,e,e,w,w,w,
e,e,e,e,e,e,e,e,
e,e,e,e,e,e,e,e,
e,e,e,e,e,e,e,e
]

sense.set_pixels(flip_image)
for flips in range (0,5): # replaced while True with a for-loop
 time.sleep(1)
 sense.flip_h()

for flips in range (0,5): # try vertical flips as well
 time.sleep(1)
 sense.flip_v()


# pp 28-29 -----------------------------------------------------------
sense.set_rotation(UP) #my own code

for measurments in range (0,1): # replaced while True with a for-loop
    t = sense.get_temperature()
    p = sense.get_pressure()
    h = sense.get_humidity()
    t = round(t, 1)
    p = round(p, 1)
    h = round(h, 1)
    msg = "Temperature = %s, Pressure=%s, Humidity=%s" % (t,p,h)
    sense.show_message(msg, scroll_speed=0.05)

sense.set_pixels(blank_image)
time.sleep(sleep_time)

# pp 29-30 -----------------------------------------------------------
for measurments in range (0,1): # replaced while True with a for-loop
    t = sense.get_temperature()
    p = sense.get_pressure()
    h = sense.get_humidity()

    t = round(t, 1)
    p = round(p, 1)
    h = round(h, 1)

    if t > 18.3 and t < 26.7:
        bg = [0, 100, 0] # green
    else:
        bg = [100, 0, 0] # red
    
    msg = "Temperature = %s, Pressure=%s, Humidity=%s" % (t, p, h)
    sense.show_message(msg, scroll_speed=0.05, back_colour=bg)

sense.set_pixels(blank_image)
time.sleep(sleep_time)

