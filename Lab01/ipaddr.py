#!/usr/bin/python
# =============================================================================
#        File : ipaddr.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#      Author : Drew Gislsason
#        Date : 3/1/2017
#
# Mods by Leo Salemann, 1/8/18:
# o Display a an image on startup, allow user to tilt the PI until image
#   orients top-up.  This ensures text will print rightside up.
# o Use subprocess instead of socket to print IP adress
# o Use subprocess to print wifi name
# o Note: this make things a bit linux-specific, but we're on Rasbian so YOLO
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#

from sense_hat import SenseHat
import time

sense = SenseHat()

# ============================== GLOBAL VARIABLES ==================================

# -----------------------< color nicknames  >-------------------
r = [255, 0, 0]
o = [255, 127, 0]
y = [255, 255, 0]
g = [0, 255, 0]
b = [0, 0, 255]
i = [75, 0, 130]
v = [159, 0, 255]
e = [0, 0, 0] # e is for empty (not black, b already used for blue)
w = [255, 255, 255]

# Blue sky, yellow sun in upper-left, triangular tree on ground.
tree_image = [
    y,y,b,b,b,b,b,b,
    y,b,b,b,b,b,b,b,
    b,b,b,b,g,b,b,b,
    b,b,b,g,g,g,b,b,
    b,b,g,g,g,g,g,b,
    b,g,g,g,g,g,g,g,
    b,b,b,g,g,g,b,b,
    g,g,g,g,g,g,g,g ]   

# -----< handy for blanking out the screen between images >-------
blank_image = [
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e,
    e,e,e,e,e,e,e,e ]

UP = 0 # For orienting text display.

# ============================== GLOBAL VARIABLES ==================================

# Show the sun/ground/fower image, allow time for user to see which side is up
# and orient the Pi properly
sense.set_pixels(tree_image)
time.sleep(1.5)

# New stuff, based on acellerometer passage in pp31-32
for ticks in range (0,100):
    x, y, z = sense.get_accelerometer_raw().values()
    x = round(x, 0)
    y = round(y, 0)

    # check x and y rotations, look for +/- 1  
    # Use shoudl be able to 'tilt' RPi on any edge, and have
    # the letters auto-orient to establish "up"
    if x == -1:
        UP = 180
    elif x == 1:
        UP = 0
    elif y == 1:
        UP = 270
    elif y == -1:
        UP = 90

sense.set_rotation(UP)


# give time to get on the Wi-Fi network
time.sleep(10)

import subprocess
wifi_name = subprocess.check_output("iwgetid").split(":")[1].split('"')[1]
# print(wifi_name)

# from stack overflow
# https://raspberrypi.stackexchange.com/questions/73772/displaying-ip-on-lcd-display
addr = subprocess.check_output(["hostname", "-I"]).split()[0]
# print(addr)

sense.show_message(str(addr))
sense.show_message(str(wifi_name))
sense.set_pixels(blank_image)

# # while True:
# Runthrus = 5
# while Runthrus > 0
#   sense.show_message(str(addr))
#   sense.show_message(str(wifi_name))
#   sense.set_pixels(tree_image)
#   time.sleep(15)
#   Runthrus--
