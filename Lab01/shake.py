##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT210
# Lab 1: Sense Hat, from Essentials_SensHAT_v1.pdf
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 1/9/18

# Code snippets based on, "Experiment with the Sense Hat", 
# The MagPi Essenstials, by The Pi Education Team
# rasberrypi.org/magpi
##############################################################

# pg 33
from sense_hat import SenseHat
sense = SenseHat()
while True:
    x, y, z = sense.get_accelerometer_raw().values()
    x = abs(x)
    y = abs(y)
    z = abs(z)
    if x > 1 or y > 1 or z > 1:
        # Ampersand easier than "!" to see
        sense.show_letter("&", text_colour=[255, 0, 0])
    else:
        sense.clear()