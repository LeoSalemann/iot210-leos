##############################################################
# UNIVERSITY OF WASHINGTON PROFESSIONAL & CONTINUING EDUCATION
# Internet of Things Certificate Program IOT210
# Lab 1: Sense Hat, from Essentials_SensHAT_v1.pdf
# Leo Salemann leos@uw.edu leo.salemann@me.com
# 1/9/18

# Code snippets based on, "Experiment with the Sense Hat", 
# The MagPi Essenstials, by The Pi Education Team
# rasberrypi.org/magpi
##############################################################

from sense_hat import SenseHat
sense = SenseHat()

# pp 31-32 --------------------------------------------------

while True:
    x, y, z = sense.get_accelerometer_raw().values()
    x=round(x, 0)
    y=round(y, 0)
    z=round(z, 0)
    print("x=%s, y=%s, z=%s" % (x, y, z))