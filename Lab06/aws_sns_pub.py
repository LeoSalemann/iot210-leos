#!/usr/bin/python
# =============================================================================
#        File : aws_sns_sub.py
# Description : Processes Simple Queue Service messages
#      Author : Drew Gislsason
#        Date : 5/25/2017
# =============================================================================
import boto3
import sys

ARN = 'arn:aws:sns:us-west-2:356792116770:iot210sns'

if len(sys.argv) > 1:
  message = sys.argv[1]
else:
  message = 'SNS hello world'

sns = boto3.client('sns')

response = sns.client.publish(TopicArn=ARN,Message=message)
print response
