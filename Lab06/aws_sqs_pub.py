#!/usr/bin/python
# =============================================================================
#        File : aws_sqs_sub.py
# Description : Processes Simple Queue Service messages
#      Author : Drew Gislsason
#        Date : 5/25/2017
# =============================================================================
import boto3
import sys

if len(sys.argv) > 1:
  message = sys.argv[1]
else:
  message = 'hello world'

# Get the service resource
sqs = boto3.resource('sqs')

# Get the queue
queue = sqs.get_queue_by_name(QueueName='iot210sqs')
print "Sending to queue: " + str(queue.url)

# Create a new message
response = queue.send_message(MessageBody=message)

# The response is NOT a resource, but gives you a message ID and MD5
print(response.get('MessageId'))
