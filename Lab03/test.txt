#### PROTOTBUF
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/protobuf' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'PUT' -d 'CgFB' -H 'Content-Type: application/protobuf' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'GET' -H 'Content-Type: application/protobuf' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/protobuf' --verbose


#### XML
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/xml' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'PUT' -d '<letter>?</letter>' -H 'Content-Type: application/xml' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'GET' -H 'Content-Type: application/xml' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/xml' --verbose


#### JSON
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/json' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'PUT' -d '{"letter":"J"}' -H 'Content-Type: application/json' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'GET' -H 'Content-Type: application/json' --verbose
curl 192.168.1.15:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/json' --verbose