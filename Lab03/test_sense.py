#!/usr/bin/python
# =============================================================================
#        File : test_sense.py
# Description : Test harness for sense.py
#      Author : Leo Salemann
#        Date : 1/18/2019
# =============================================================================
from sense import PiSenseHat

# create Pi SenseHat Object
sense_hat = PiSenseHat()

print ("===================< BEGIN ENVIRONMENTAL METHODS >===================")

print ("  Get all environment values.")
print (sense_hat.getAllSensors())

print ("================< ENVIRONMENTAL METHODS COMPLETE >===================")
print ("")

print ("===================< BEGIN LED METHODS >===================")

print ("  Clear pixels")
sense_hat.clearPixels()

print ("  Display the letter J") 
sense_hat.printLetter("J")

print ("================< LED METHODS COMPLETE >===================")