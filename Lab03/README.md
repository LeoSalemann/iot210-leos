# HW3 Multi Server
## Leo Salemann

Provides DELETE, PUT, GET access to send letters to/from SenseHAT LED screen.

api/v1/led Examples:

#### PROTOTBUF
curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/protobuf' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'PUT' -d 'CgFB' -H 'Content-Type: application/protobuf' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'GET' -H 'Content-Type: application/protobuf' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/protobuf' --verbose


#### XML
curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/xml' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'PUT' -d '<letter>?</letter>' -H 'Content-Type: application/xml' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'GET' -H 'Content-Type: application/xml' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/xml' --verbose


#### JSON
curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/json' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'PUT' -d '{"letter":"J"}' -H 'Content-Type: application/json' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'GET' -H 'Content-Type: application/json' --verbose

curl <RPI IP ADDRESS or URL>:5000/api/v1/led -X 'DELETE' -H 'Content-Type: application/json' --verbose